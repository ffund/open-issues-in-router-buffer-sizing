import xmltodict
import sys

if len(sys.argv)!=3:
  print "\nThis script gives you the clusterssh command and SCP commands for the "
  print "experiment on reproducing 'Open Issues in Router Buffer Sizing'.\n\n"
  print "Usage: python %s /path/to/manifest/rspec geniUsername\n" % sys.argv[0]
  sys.exit()

filename=sys.argv[1]
username=sys.argv[2]

with open(filename) as fd:
    doc = xmltodict.parse(fd.read())

print "Your clusterssh command (for sending commands to all source nodes) is:\n"
sys.stdout.write('cssh --username ')
sys.stdout.write(username)

for d in doc['rspec']['node']:
  if d['@client_id'].startswith("h"):
    sys.stdout.write(' ')
    sys.stdout.write(d['services']['login'][0]['@hostname'])
    sys.stdout.write(':')
    sys.stdout.write(d['services']['login'][0]['@port'])

print "\n\nYour SCP commands (for transferring data files from source nodes) are:\n"



for d in doc['rspec']['node']:
  if d['@client_id'].startswith("h"):
    sys.stdout.write('scp -P ')
    sys.stdout.write(d['services']['login'][0]['@port'])
    sys.stdout.write(' ')
    sys.stdout.write(username)
    sys.stdout.write('@')
    sys.stdout.write(d['services']['login'][0]['@hostname'])
    print ":~/dat-* . "



