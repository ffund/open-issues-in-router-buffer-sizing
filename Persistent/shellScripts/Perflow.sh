# eth0 => 10.1.2.1, eth1 => 10.2.1.1
# remove any that don't start with 10
# Reference: https://github.com/dolly115/resilient-networks-lab/
declare -A arr
declare -A brr
 
for i in $(ifconfig | grep "eth" | awk '{print $1;}')
do 
  IP=$(ifconfig $i | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')
  if [[ $IP == 10.* ]]
  then
    arr["$i"]=$IP
    brr["$IP"]=$i
	# Get second octet of IP address (which is in $IP variable)	
	# store it in a variable called type
	intialTime=$(date +"%s")
	simulationTime=150
	type=`echo $IP | cut -d . -f 2`
	type1=`echo $IP | cut -d . -f 3`
	type2=`echo $IP | cut -d . -f 4`

	#node 1 traffic

	if [[ $type -eq 1 && $type1 -eq 10 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10000
		max_n=13
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10250
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 2 traffic

	elif [[ $type -eq 1 && $type1 -eq 11 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10014
		max_n=13
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10251
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 3 traffic

	elif [[ $type -eq 1 && $type1 -eq 12 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10026
		max_n=13
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10252
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 4 traffic

	elif [[ $type -eq 2 && $type1 -eq 10 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10039
		max_n=12
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10253
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 5 traffic

	elif [[ $type -eq 2 && $type1 -eq 11 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10051
		max_n=12
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10254
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 6 traffic

	elif [[ $type -eq 2 && $type1 -eq 20 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10063
		max_n=12
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10255
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 7 traffic

	elif [[ $type -eq 2 && $type1 -eq 30 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10075
		max_n=12
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10256
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 8 traffic

	elif [[ $type -eq 2 && $type1 -eq 31 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10087
		max_n=12
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10257
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 9 traffic

	elif [[ $type -eq 3 && $type1 -eq 10 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10099
		max_n=10
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10258
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 10 traffic

	elif [[ $type -eq 3 && $type1 -eq 11 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10109
		max_n=10
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10259
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 11 traffic

	elif [[ $type -eq 3 && $type1 -eq 20 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10119
		max_n=10
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10260
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 12 traffic

	elif [[ $type -eq 3 && $type1 -eq 21 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10129
		max_n=10
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10261
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 13 traffic

	elif [[ $type -eq 3 && $type1 -eq 30 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10139
		max_n=10
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10262
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 14 traffic

	elif [[ $type -eq 3 && $type1 -eq 31 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10149
		max_n=10
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10263
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 15 traffic

	elif [[ $type -eq 3 && $type1 -eq 40 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10159
		max_n=10
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10264
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 16 traffic

	elif [[ $type -eq 3 && $type1 -eq 41 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10169
		max_n=10
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10265
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 17 traffic

	elif [[ $type -eq 3 && $type1 -eq 50 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10179
		max_n=10
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10266
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done

	#node 18 traffic

	elif [[ $type -eq 3 && $type1 -eq 51 && $type2 -eq 1 ]];
	then
		currentTime=$(date +"%s")
		port=10179
		max_n=10
		pakLen=1500
        n=1
        while [[ $n -le $max_n ]];
        do 
            iperf3 -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
		runTime=$(($currentTime-$intialTime))
		while [[ $runTime -le $simulationTime ]];
		do 
			miceport=10267
			bytes=$(shuf -i7500-37500 -n1)
			iperf3 -c 10.10.1.1 -p $miceport -l $pakLen -n $bytes
			sleeptime=$(shuf -i1-2 -n1)
			sleep $sleeptime
			currentTime=$(date +"%s")
			runTime=$(($currentTime-$intialTime))
		done	
	fi

  fi
done

for key in ${!arr[@]}
do 
  echo ${key} ${arr[${key}]} 
done

