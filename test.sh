# eth0 => 10.1.2.1, eth1 => 10.2.1.1
# remove any that don't start with 10
# Reference: https://github.com/dolly115/resilient-networks-lab/
declare -A arr
declare -A brr
 
for i in $(ifconfig | grep "eth" | awk '{print $1;}')
do 
  IP=$(ifconfig $i | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')
  if [[ $IP == 10.* ]]
  then
    arr["$i"]=$IP
    brr["$IP"]=$i
	# Get second octet of IP address (which is in $IP variable)	
	# store it in a variable called type
	intialTime=$(date +"%s")
	simulationTime=150
	type=`echo $IP | cut -d . -f 2`
	type1=`echo $IP | cut -d . -f 3`
	type2=`echo $IP | cut -d . -f 4`

	#node 1 traffic

	if [[ "$type" -eq 1 && "$type1" -eq 10 && "$type2" -eq 1 ]];
	then
		n=1
		n1=13
		n2=20
		n3=7
		port1=10001
		port2=10201
		port3=10601 
		while [[ "$n" -le "$n1" ]];
		do
		bash flow.sh 200 5 "$port1" &
        done
    fi
   fi
done
    
