# eth0 => 10.1.2.1, eth1 => 10.2.1.1
# remove any that don't start with 10
# Reference: https://github.com/dolly115/resilient-networks-lab/
declare -A arr
declare -A brr
 
for i in $(ifconfig | grep "eth" | awk '{print $1;}')
do 
  IP=$(ifconfig $i | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')
  if [[ $IP == 10.* ]]
  then
    arr["$i"]=$IP
    brr["$IP"]=$i
	# Get second octet of IP address (which is in $IP variable)	
	# store it in a variable called type
	intialTime=$(date +"%s")
	simulationTime=150
	type=`echo $IP | cut -d . -f 2`
	type1=`echo $IP | cut -d . -f 3`
	type2=`echo $IP | cut -d . -f 4`

	#node 1 traffic

	if [[ $type -eq 1 && $type1 -eq 10 && $type2 -eq 1 ]];
	then
		n=1
		n1=13
		n2=20
		n3=7
		port1=10001
		port2=10201
		port3=10601 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done

	#node 2 traffic
	elif [[ $type -eq 1 && $type1 -eq 11 && $type2 -eq 1 ]];
	then
		n=1
		n1=13
		n2=20
		n3=7
		port1=10013
		port2=10221
		port3=10608 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 3 traffic
	elif [[ $type -eq 1 && $type1 -eq 12 && $type2 -eq 1 ]];
	then
		n=1
		n1=13
		n2=20
		n3=7
		port1=10026
		port2=10241
		port3=10615 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 4 traffic
	elif [[ $type -eq 2 && $type1 -eq 10 && $type2 -eq 1 ]];
	then
		n=1
		n1=12
		n2=20
		n3=7
		port1=10039
		port2=10261
		port3=10622 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 5 traffic
	elif [[ $type -eq 2 && $type1 -eq 11 && $type2 -eq 1 ]];
	then
		n=1
		n1=12
		n2=20
		n3=7
		port1=10051
		port2=10281
		port3=10629 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 6 traffic
	elif [[ $type -eq 2 && $type1 -eq 20 && $type2 -eq 1 ]];
	then
		n=1
		n1=12
		n2=20
		n3=7
		port1=10063
		port2=10301
		port3=10636 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 7 traffic
	elif [[ $type -eq 2 && $type1 -eq 30 && $type2 -eq 1 ]];
	then
		n=1
		n1=12
		n2=20
		n3=7
		port1=10075
		port2=10321
		port3=10643 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 8 traffic
	elif [[ $type -eq 2 && $type1 -eq 31 && $type2 -eq 1 ]];
	then
		n=1
		n1=12
		n2=20
		n3=7
		port1=10087
		port2=10341
		port3=10650 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 9 traffic
	elif [[ $type -eq 3 && $type1 -eq 10 && $type2 -eq 1 ]];
	then
		n=1
		n1=10
		n2=20
		n3=7
		port1=10099
		port2=10361
		port3=10657 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 10 traffic
	elif [[ $type -eq 3 && $type1 -eq 11 && $type2 -eq 1 ]];
	then
		n=1
		n1=10
		n2=20
		n3=7
		port1=10109
		port2=10381
		port3=10664 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 11 traffic
	elif [[ $type -eq 3 && $type1 -eq 20 && $type2 -eq 1 ]];
	then
		n=1
		n1=10
		n2=20
		n3=7
		port1=10119
		port2=10401
		port3=10671 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 12 traffic
	elif [[ $type -eq 3 && $type1 -eq 21 && $type2 -eq 1 ]];
	then
		n=1
		n1=10
		n2=20
		n3=7
		port1=10129
		port2=10421
		port3=10678 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 13 traffic
	elif [[ $type -eq 3 && $type1 -eq 30 && $type2 -eq 1 ]];
	then
		n=1
		n1=10
		n2=20
		n3=7
		port1=10139
		port2=10441
		port3=10685 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 14 traffic
	elif [[ $type -eq 3 && $type1 -eq 31 && $type2 -eq 1 ]];
	then
		n=1
		n1=10
		n2=20
		n3=7
		port1=10149
		port2=10461
		port3=10692 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 15 traffic
	elif [[ $type -eq 3 && $type1 -eq 40 && $type2 -eq 1 ]];
	then
		n=1
		n1=10
		n2=20
		n3=7
		port1=10159
		port2=10481
		port3=10699 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 16 traffic
	elif [[ $type -eq 3 && $type1 -eq 41 && $type2 -eq 1 ]];
	then
		n=1
		n1=10
		n2=20
		n3=7
		port1=10169
		port2=10501
		port3=10706 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 17 traffic
	elif [[ $type -eq 3 && $type1 -eq 50 && $type2 -eq 1 ]];
	then
		n=1
		n1=10
		n2=20
		n3=7
		port1=10179
		port2=10521
		port3=10713 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	#node 18 traffic
	elif [[ $type -eq 3 && $type1 -eq 51 && $type2 -eq 1 ]];
	then
		n=1
		n1=10
		n2=20
		n3=7
		port1=10189
		port2=10541
		port3=10720 
		while [[ $n -le $n1 ]];
		do
		bash /flow.sh 200 5 $port1 &
		port1=$(($port1+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
		bash /flow.sh 50 5 $port2 &
		port2=$(($port2+1))
		n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
		bash /flow.sh 15 5 $port3 &
		port3=$(($port3+1))
		n=$(($n+1))
		done
	fi
  fi
done

for key in ${!arr[@]}
do 
  echo ${key} ${arr[${key}]} 
done

