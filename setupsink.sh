#Install iperf3
wget https://iperf.fr/download/ubuntu/libiperf0_3.1.2-1_amd64.deb
wget https://iperf.fr/download/ubuntu/iperf3_3.1.2-1_amd64.deb
sudo dpkg -i libiperf0_3.1.2-1_amd64.deb iperf3_3.1.2-1_amd64.deb
#Get all the resources at node18
#test scripts for closed loop
wget https://bitbucket.org/rajeevpnj/el7353finalproject/raw/master/closedLoop/shellScripts/sink/receiverCL.sh
#test script for persistent flow
wget https://bitbucket.org/rajeevpnj/el7353finalproject/raw/master/Persistent/shellScripts/sink/receiverPer.sh
sudo apt-get -y install tcpdump vim iperf
